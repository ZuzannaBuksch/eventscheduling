// Class that includes informations about transmission channel

#ifndef CHANNEL_H
#define CHANNEL_H

#include <vector>
#include "packet.h"

class Channel
{
public:
	Channel();
	~Channel();

	bool GetChannelStatus();
	void SetChannelStatus(bool status);

	bool GetCollisionStatus();
	void SetCollisionStatus(bool status);

	bool GetSentAck();
	void SetSentAck(bool status);

	std::vector<Packet*>* GetPacketsCurrentlyTransmitting();

private:
	std::vector<Packet*>packets_currently_transmitting_; // vector of packets which is in channel

	bool channel_status = true; // true - no packet in channel, false - packet/packets in channel
	bool collision_status = false; // true - collision detected
	bool sent_ack = false;


};


#endif