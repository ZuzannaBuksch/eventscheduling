#ifndef GENERATE_PACKET_EVENT_H
#define GENERATE_PACKET_EVENT_H

#include "event.h"
#include "logger.h"
#include "wireless_network.h"
#include "check_channel_status_event.h"
//#include "transmitter.h"

class Transmitter;

class GeneratePacketEvent 
	: public Event
{
public:

	GeneratePacketEvent(size_t time, WirelessNetwork* wireless_network, EventList* event_list, Logger* logger_ptr, int id);
	~GeneratePacketEvent();

	void Execute() override;

private:
	Logger* logger_ptr_ = nullptr;
	EventList* event_list_ = nullptr;
	int transmitter_id; // id nadajnika, dla kt�rego wykonywane jest event 
	const size_t max_generate_packet_time = 100; // const k.... zmieni� nazw� 
	const size_t max_transmission_time = 100;


};


#endif GENERATE_PACKET_EVENT_H
