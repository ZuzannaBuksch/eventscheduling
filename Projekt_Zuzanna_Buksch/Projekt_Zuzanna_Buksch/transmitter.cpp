#include "transmitter.h"

Transmitter::Transmitter(int id)
{
  transmitter_id = id;
}

Transmitter::~Transmitter()
{
}

int Transmitter::GetTransmitterId()
{
  return transmitter_id; 
}

void Transmitter::SetTransmitterId(int tx_id)
{
    transmitter_id = tx_id;
}


size_t Transmitter::GetWaitingTime()
{
  return waiting_time;
}

void Transmitter::SetWaitingTime(size_t difs)
{
  waiting_time = difs;
}

size_t Transmitter::GetAckTime()
{
  return ack_time;
}

bool Transmitter::GetAckReceved()
{
  return ack_receved;
}

void Transmitter::SetAckReceved(bool ack_status)
{
  ack_receved = ack_status;
}


bool Transmitter::GetCheckStatus()
{
  return check_status;
}

void Transmitter::SetCheckStatus(bool status)
{
  check_status = status;
}

std::queue<Packet*>* Transmitter::GetPackets()
{
  return &packets_;
}

void Transmitter::GeneratePacket(Logger *logger, int temp)
{
  static size_t id = 0; // id nowego pakietu
  ++id;
  auto packet = new Packet(id, std::rand() % 10 *10 +10, temp); // czas trwania pakietu 1, 2, ... 10ms -> 10, 20, ..., 100
  packets_.push(packet);
  logger->Info("Transmitter "+ std::to_string(temp)+ ", generated packet id: "+ std::to_string(id)+ ", packet transmission time: "+ std::to_string(packets_.back()->GetCtpkTime()));

}

void Transmitter::StartTransmission(Logger* logger, Packet* packet, WirelessNetwork* wn_ptr)
{
  logger->Info("Started transmission of packet "+ std::to_string(packets_.front()->GetPacketId()));
  wn_ptr->GetChannel()->GetPacketsCurrentlyTransmitting()->push_back(packet);
  wn_ptr->GetChannel()->SetChannelStatus(false);
  if (wn_ptr->GetChannel()->GetPacketsCurrentlyTransmitting()->size() > 1)
  {
    wn_ptr->GetChannel()->SetCollisionStatus(true);
  }
  else
  {
    wn_ptr->GetChannel()->SetCollisionStatus(false);
  }
  
}

  


