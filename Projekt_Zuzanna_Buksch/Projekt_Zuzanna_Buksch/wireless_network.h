#ifndef WIRELESS_NETWORK_H
#define WIRELESS_NETWORK_H

//Class that includes all system components
class Transmitter;
class Receiver;
class Channel;

#include<vector>

#include "transmitter.h"
#include "receiver.h"
#include "channel.h"


class WirelessNetwork
{
public:
	WirelessNetwork();
	~WirelessNetwork();

	std::vector<Transmitter*>* GetTransmitters();
	std::vector<Receiver*>* GetReceivers();
	Channel* GetChannel();

	const int kNumberOfTransmitters = 8; // Number of transmitters and receivers in wireless network

private:
	std::vector<Transmitter*>transmitters_; // Vector of transmitters
	std::vector<Receiver*>receivers_; // Vector of receivers
	Channel channel;
};

#endif // WIRELESS_NETWORK_H

