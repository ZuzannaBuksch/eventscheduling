// Class that includes informations about receiver
#ifndef RECEIVER_H
#define RECEIVER_H

#include "channel.h"
#include "logger.h"

class Channel;

class Receiver
	: public Channel
{
public:
	Receiver(int id);
	~Receiver();

	bool GetTerStatus() { return ter_status; }
	void SetTerStatus(bool ter_error) { ter_status = ter_error; }


	//metody

private:
	int receiver_id; // transmitter identification number
	bool ter_status; // true - ter detected

};


#endif

