#ifndef EVENT_H
#define EVENT_H

#include <queue>
#include "wireless_network.h"
#include <functional>

//class WirelessNetwork; // ALBO INCLUDE????

class Event
{
public:
	typedef std::priority_queue<Event*, std::vector<Event*>, std::function<bool(Event*, Event*)>> EventList;//kolejka priorytetowa zdarzee� czasowych
	
	explicit Event(size_t time, WirelessNetwork* wireless_network_ptr); // doda� event_list???
	~Event();

	virtual void Execute()=0;

	size_t get_time() { return time_; }

protected:
	size_t time_;
	WirelessNetwork* wireless_network_ = nullptr;
	//Transmitter* transmitter_ptr_ = nullptr;

};



#endif EVENT_H