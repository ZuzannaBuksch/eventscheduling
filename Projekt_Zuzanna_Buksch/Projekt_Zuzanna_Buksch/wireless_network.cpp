#include "wireless_network.h"

WirelessNetwork::WirelessNetwork()
{
  auto channel = new Channel();

  for (int i = 0; i < kNumberOfTransmitters; ++i)
  {
    auto transmitter = new Transmitter(i);
    transmitters_.push_back(transmitter);
    auto receiver = new Receiver(i);
    receivers_.push_back(receiver);
  }
}

WirelessNetwork::~WirelessNetwork()
{
}

std::vector<Transmitter*>* WirelessNetwork::GetTransmitters()
{
  return &transmitters_;
}

std::vector<Receiver*>* WirelessNetwork::GetReceivers()
{
  return &receivers_;
}

Channel* WirelessNetwork::GetChannel()
{
  return &channel;
}






