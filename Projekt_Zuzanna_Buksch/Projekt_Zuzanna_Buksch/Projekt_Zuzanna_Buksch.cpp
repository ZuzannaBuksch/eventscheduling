// Projekt_Zuzanna_Buksch.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <queue>
#include <time.h>

#include "generate_packet_event.h"
#include "check_channel_status_event.h"
#include "end_transmission_packet_event.h"
#include "end_transmission_ack_event.h"
#include "transmitter.h"
#include "receiver.h"
#include "channel.h"
#include "packet.h"
#include "logger.h"

#include "wireless_network.h"
#include "simulator.h"

int main()
{
  std::srand((int)time(NULL)); 
	std::cout << "Event Scheduling, Protocol A6e"<< std::endl;
  auto wnetwork = new WirelessNetwork();
	Simulator simulator = Simulator(wnetwork);

	simulator.M2(2000);
}

