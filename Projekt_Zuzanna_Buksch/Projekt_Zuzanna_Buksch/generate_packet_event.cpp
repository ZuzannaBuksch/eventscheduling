#include "generate_packet_event.h"

#include<iostream>
#include<string>
#include<cstdlib>
#include<queue>

GeneratePacketEvent::GeneratePacketEvent(size_t time, WirelessNetwork* wireless_network, EventList* event_list, Logger* logger_ptr, int id)
  :Event(time, wireless_network), event_list_(event_list), logger_ptr_(logger_ptr), transmitter_id(id)
{
}

GeneratePacketEvent::~GeneratePacketEvent()
{
}

void GeneratePacketEvent::Execute()
{
  wireless_network_->GetTransmitters()->at(transmitter_id)->GeneratePacket(logger_ptr_, transmitter_id);
  event_list_->push(new GeneratePacketEvent(time_ + rand() %max_generate_packet_time, wireless_network_,event_list_, logger_ptr_,rand()%8));
  
  // je�li w buforze znajduj� si� pakiety do wys�ania i nadajnik wcze�niej nie rozpocz�� nas�uchiwania
  if (!wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->empty() && wireless_network_->GetTransmitters()->at(transmitter_id)->GetCheckStatus() == false)
  {
    event_list_->push(new CheckChannelStatusEvent(time_, wireless_network_, event_list_, logger_ptr_, transmitter_id)); // rozpocznij nas�uchiwanie kana�u
  }
}
