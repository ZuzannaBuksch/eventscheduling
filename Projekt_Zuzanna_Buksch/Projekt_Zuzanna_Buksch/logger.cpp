#include "logger.h"
#include <iostream>

Logger::Logger()
{
}

Logger::~Logger()
{
}

void Logger::Info(std::string message)
{
 if(level_ != Level::Error)
  std::cout << "[Info] " << message << std::endl;
}

void Logger::Error(std::string message)
{
  std::cout << "[Error] " << message << std::endl;
}
