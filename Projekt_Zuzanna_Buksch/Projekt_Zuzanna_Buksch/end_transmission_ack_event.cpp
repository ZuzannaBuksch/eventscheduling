#include "end_transmission_ack_event.h"

EndTransmissionAckEvent::EndTransmissionAckEvent(size_t time, WirelessNetwork* wireless_network, EventList* event_list, Logger* logger_ptr, int id)
  :Event(time, wireless_network), event_list_(event_list), logger_ptr_(logger_ptr), transmitter_id(id)
{
}

EndTransmissionAckEvent::~EndTransmissionAckEvent()
{
}

void EndTransmissionAckEvent::Execute()
{
	if (wireless_network_->GetChannel()->GetSentAck() == true)
	{
		logger_ptr_->Info("Ended transmission of ACK: "+ std::to_string(wireless_network_->GetChannel()->GetPacketsCurrentlyTransmitting()->front()->GetPacketId()));
		
		wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->pop();
		wireless_network_->GetChannel()->GetPacketsCurrentlyTransmitting()->pop_back();
		wireless_network_->GetChannel()->SetChannelStatus(true);
		
	}

	else
	{
		logger_ptr_->Info("No receved ACK");

		int lr_number_temp = wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->GetLrNumber() + 1;
		if (lr_number_temp < 11) // je�li liczba retransmisji jest <= LR (10)
		{
			wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->SetLrNumber(lr_number_temp);
			int R = rand() % int(std::pow(2,(lr_number_temp))-1);
			logger_ptr_->Info("R: " + std::to_string(R));
			int crp_time = R * (wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->GetCtpkTime());
			logger_ptr_->Info("LR: " + std::to_string(wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->GetLrNumber()));
			event_list_->push(new CheckChannelStatusEvent(time_ + (crp_time), wireless_network_, event_list_, logger_ptr_, transmitter_id)); // zaplanuj nowe zdarzenie nas�uchiwania kana�u 
		}
		else
		{
			wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->pop(); // usu� pakiet z bufora 
		}

	}
	
	//logger_ptr_->Info("ilosc pakietow w buforze " +std::to_string(transmitter_id)+ " to "+ std::to_string(wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->size()));
}
