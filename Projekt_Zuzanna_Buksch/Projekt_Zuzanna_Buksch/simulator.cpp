#include "simulator.h"


Simulator::Simulator(WirelessNetwork* wieless_network_ptr): wireless_network_(wieless_network_ptr)
{
}

Simulator::~Simulator()
{
}

void Simulator::StepInto()
{
  char key;
  std::cout << "\nPress ENTER to continue..." << std::endl;
  key = getchar();
}

bool Simulator::SelectMode(int mode)
{
  if (mode == 1)
    return true;
  else if (mode == 2)
    return false;
  else
  {
    std::cout<<"Select 1 or 2. \n";
    int temp;
    std::cin >> temp;
    SelectMode(temp);
  }
 
}


void Simulator::M2(int time)
{
  clock_ = 0;
  int select_mode;

  auto logger = Logger();
  logger.SetLevel(Logger::Level::Info);

  std::cout << "Started Simulation method M2: \n";
  std::cout << "\nSelect simulation mode: \n1. Step into. \n2. Step over. \n";
  std::cin >> select_mode;
  
  auto cmp = [](Event* left, Event* right) { return left->get_time() > right->get_time(); };
  Event::EventList event_list(cmp);

  event_list.push(new GeneratePacketEvent(rand() % generate_packet_max_time, wireless_network_, &event_list, &logger, rand()%8));

  while (clock_ < static_cast<size_t>(time))
  {
    Event* event = event_list.top();
    event_list.pop();
    clock_ = event->get_time();
    if (SelectMode(select_mode) == true)
      StepInto();
    std::cout << "Simulation Time: " << clock_ << "\n";
    event->Execute();

  }

}
