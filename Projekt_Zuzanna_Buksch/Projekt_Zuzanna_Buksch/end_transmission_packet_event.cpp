#include "end_transmission_packet_event.h"

EndTransmissionPacketEvent::EndTransmissionPacketEvent(size_t time, WirelessNetwork* wireless_network, EventList* event_list, Logger* logger_ptr, int id)
  :Event(time, wireless_network), event_list_(event_list), logger_ptr_(logger_ptr), transmitter_id(id)
{
}

EndTransmissionPacketEvent::~EndTransmissionPacketEvent()
{
}

void EndTransmissionPacketEvent::Execute()
{
	int ter_table[] = {1, 0, 1, 1, 1}; // ter - prawdopodobie�stwo, �e pakiet wy�le si� niepoprawnie - 20%
	int temp = rand() % 5; // zmienna pomocnicza do wyznaczenia b��du ter 

	if (wireless_network_->GetChannel()->GetCollisionStatus() == true || ter_table[temp] == 0) // je�li wyst�pi�a kolizja lub b��d ter 
	{
		logger_ptr_->Info("Collision or TER detected");
		wireless_network_->GetChannel()->SetSentAck(false);
		wireless_network_->GetChannel()->GetPacketsCurrentlyTransmitting()->pop_back();

		if (wireless_network_->GetChannel()->GetPacketsCurrentlyTransmitting()->empty()) // w przypadku kolizji kana� zostanie zwolniony po czasie trwania najd�u�eszgo pakietu
		{
			wireless_network_->GetChannel()->SetChannelStatus(true);
			wireless_network_->GetChannel()->SetCollisionStatus(false);
		}
		event_list_->push(new EndTransmissionAckEvent(time_ + 10, wireless_network_, event_list_, logger_ptr_, transmitter_id)); // zaplanowanie 
	}
	else
	{
		logger_ptr_->Info("Ended transmission of packet");
		wireless_network_->GetChannel()->SetSentAck(true); // ustaw falg� oznaczaj�c� wys�anie ACK na true - wys�ano ACK
		event_list_->push(new EndTransmissionAckEvent(time_ + 10, wireless_network_, event_list_, logger_ptr_, transmitter_id));
	}

}
