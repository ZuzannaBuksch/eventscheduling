// Class that represents transmitter

#ifndef TRANSMITTER_H
#define TRANSMITTER_H

#include <queue>
#include<iostream>

#include "channel.h"
#include "packet.h"
#include "logger.h"
#include "wireless_network.h"

class WirelessNetwork;
class Channel;

class Transmitter : public Channel
{
public:
	Transmitter(int id);
	~Transmitter();

	int GetTransmitterId();
	void SetTransmitterId(int tx_id); 

	size_t GetWaitingTime();
	void SetWaitingTime(size_t w_time);

	//size_t GetStartOfTransmissionTime();
	//void SetStartOfTransmissionTime(size_t start_time);

	size_t GetAckTime();

	bool GetAckReceved();
	void SetAckReceved(bool ack_status);
	
	//bool GetRetransmissionStatus();
	//void SetRetransmissionStatus(bool ret_status);

	bool GetCheckStatus();
	void SetCheckStatus(bool status);


	std::queue<Packet*>* GetPackets();

	// metody
	void GeneratePacket(Logger* logger, int tx_id);
	void StartTransmission(Logger* logger, Packet* packet, WirelessNetwork* wn_ptr); // rozpoczyna transmisje 
	

private:
	int transmitter_id; // transmitter identification number
	size_t waiting_time = 0; // time of channel idling 
	size_t ack_time = 10; // time of waiting for ACK message
	bool ack_receved = false; // true - ack recevd
	//bool retransmission_status = true; // true - number of retransmission is less than or equal to the maximum 
																		 //number of transmission attempts (lr_number<=10) 
	bool check_status = false; // flaga określająca czy nadajnik jest w trakcie nasłuchiwania, false - nie 

	size_t crp_time; // random waiting time in case of retransmission 
	std::queue<Packet*> packets_;  // queue of packets 
	
	size_t generate_timer = -1;
	size_t transmission_timer_ = -1;

	int lost_packets; 
	int sent_packets;
	int retransmitted_packets;
};

#endif

